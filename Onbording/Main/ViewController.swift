//
//  ViewController.swift
//  Onbording
//
//  Created by Force Mobile Studios on 8/11/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var goodMorningLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        goodMorningLbl.text = "Good Morning SPC Belek"
    }

    @IBAction func ramsteinBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Open Safari", message: "This content is in a web page, do you want to continue.", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            if let url = URL(string: "https://www.ramstein.af.mil/PCS-Info/Newcomers-Info/Getting-Settled/") {
                UIApplication.shared.open(url)
            }
        }))

        self.present(alert, animated: true)
    }
    
    @IBAction func vehicleRegistration(_ sender: Any) {
        let alert = UIAlertController(title: "Open Safari", message: "This content is in a web page, do you want to continue.", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            if let url = URL(string: "https://www.ramstein.af.mil/Portals/6/Vehicle%20Registration%202_0.pdf") {
                UIApplication.shared.open(url)
            }
        }))

        self.present(alert, animated: true)
        
    }
    
    @IBAction func housingOffice(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "Open Safari", message: "This content is in a web page, do you want to continue.", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            if let url = URL(string: "https://www.ramstein.af.mil/Ramstein-Housing-Office/") {
                UIApplication.shared.open(url)
            }
        }))

        self.present(alert, animated: true)
        
        }
    
    
}

