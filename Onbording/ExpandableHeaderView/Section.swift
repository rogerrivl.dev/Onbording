//
//  Section.swift
//  Onbording
//
//  Created by Force Mobile Studios on 8/12/21.
//

import Foundation

struct Section {
    var title:String!
    var description:[String]!
    var expanded: Bool!
    
    init(title:String, description:[String], expanded:Bool) {
        self.title = title
        self.description = description
        self.expanded = expanded
    }
    
}
