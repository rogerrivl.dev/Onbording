//
//  HousingViewController.swift
//  Onbording
//
//  Created by Force Mobile Studios on 8/23/21.
//

import UIKit
import WebKit
import Network
class HousingViewController: UIViewController {
    
    let defaultUrl = "https://www.ramstein.af.mil/Ramstein-Housing-Office/"
   
    
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "InternetConnectionMonitor")

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url = URL(string: defaultUrl)
           let requestObj = URLRequest(url: url! as URL)
           webView.load(requestObj)
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("We're connected!")
            } else {
                print("No connection.")
            }

            print("Is it Expensive\(path.isExpensive)")
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        
    }
    
    
    
    
    @IBAction func backBtn(_ sender: Any) {
        if(self.webView.canGoBack) {
                self.webView.goBack()
               }
    }
    
    @IBAction func refreshBtn(_ sender: Any) {
        let url = URL(string: defaultUrl)
           let requestObj = URLRequest(url: url! as URL)
           webView.load(requestObj)
    }
    
    @IBAction func forwardBtn(_ sender: Any) {
        if(self.webView.canGoForward) {
            self.webView.goForward()
               }
    }
    
    @IBAction func openSafari(_ sender: Any) {
        if let url = URL(string: defaultUrl) {
            UIApplication.shared.open(url)
        }
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
