//
//  DirectoryViewController.swift
//  Onbording
//
//  Created by Force Mobile Studios on 8/20/21.
//
import UIKit

class DirectoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ExpandableHeaderViewDelegate {
    
    var sections = [
        Section(title:"Front Office",description: ["478-0000","480-0000"],expanded: false),
        Section(title:"CSS",description: ["478-0000","480-0000"],expanded: false),
        Section(title:"DO",description: ["478-0000","480-0000","480-9999"],expanded: false)
        
    
    
    
    ]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].description.count
    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = inProcessTableView.dequeueReusableCell(withIdentifier: "labelCell", for: indexPath)
//        cell.textLabel?.text = sections[indexPath.section].description[indexPath.row]
//
//        return cell
//
//    }
    //Possible Function
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if let cell = inProcessTableView.dequeueReusableCell(withIdentifier: "labelCell", for: indexPath) as? DirectoryTableViewCell {
        cell.dnsLbl.text = sections[indexPath.section].description[indexPath.row]
            return cell

        }



        return UITableViewCell()

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if sections[indexPath.section].expanded {
            return 90
        }else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpandableHeaderView()
        
        header.customInit(title: sections[section].title, section: section, delegate: self)
        return header
    }
    
    func toggleSection(header: ExpandableHeaderView, section: Int) {
        sections[section].expanded = !sections[section].expanded
        
        for i in 0 ..< sections[section].description.count{
            inProcessTableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        inProcessTableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Press \(indexPath.row)")
    }
    

    @IBOutlet var inProcessTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
