//
//  DirectoryTableViewCell.swift
//  Onbording
//
//  Created by Force Mobile Studios on 8/20/21.
//

import UIKit

class DirectoryTableViewCell: UITableViewCell {

    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var dnsLbl: UILabel!
    @IBOutlet var commercialLbl: UILabel!
    @IBOutlet var locationLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
